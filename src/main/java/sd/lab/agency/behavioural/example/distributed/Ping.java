package sd.lab.agency.behavioural.example.distributed;

import sd.lab.agency.behavioural.Behaviour;
import sd.lab.agency.behavioural.impl.BaseBehaviouralAgent;
import sd.lab.agency.behavioural.messages.Message;

import static sd.lab.agency.behavioural.Behaviour.receive;
import static sd.lab.agency.behavioural.Behaviour.send;

class Ping extends BaseBehaviouralAgent {
    private int i = 0;
    private final String pongName;

    public Ping(String name, String pongName) {
        super(name);
        this.pongName = pongName;
    }

    @Override
    public void setup() {
        Behaviour.send(pongName, "ping", this::onMessageSent)
                .andThen(receive(this::onMessageReceived))
                .repeatWhile(() -> i++ < 10)
                .andThen(send(pongName, "stop", this::onMessageSent))
                .andThen(() -> log("My duty here is over. Good bye"))
                .andThen(this::stop)
                .addTo(this);

    }

    private void onMessageSent(Message message) {
        log("Sent message %s", message);
    }

    private void onMessageReceived(Message message) {
        log("Received message %s", message);
        if (!(message.getSender().startsWith(pongName) && message.getContent().equalsIgnoreCase("pong"))) {
            log("Wrong content or sender, I'm out.");
            stop();
        }
    }
}
