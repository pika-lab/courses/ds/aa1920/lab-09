package sd.lab.agency.behavioural.example.distributed;

import sd.lab.agency.Environment;
import sd.lab.agency.impl.DistributedEnvironment;
import sd.lab.ws.presentation.MIMETypes;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class DistributedPingPong {
    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {
        Environment env = new DistributedEnvironment(
                "env",
                args[0],
                Integer.parseInt(args[1]),
                MIMETypes.APPLICATION_JSON,
                "admin",
                "admin"
        );

        final String pingName = "Ping";
        final String pongName = "Pong";

        if (args[2].equalsIgnoreCase("ping")) {

            env.createAgent(Ping.class, pingName, pongName).start();
        } else if (args[2].equalsIgnoreCase("pong")) {
            env.createAgent(Pong.class, pongName, pingName).start();
        } else {
            throw new IllegalStateException("Invalid option: " + args[3]);
        }

        env.awaitAllAgentsStop(Duration.ofHours(1))
            .shutdown()
            .awaitShutdown(Duration.ofSeconds(1));
    }


}




