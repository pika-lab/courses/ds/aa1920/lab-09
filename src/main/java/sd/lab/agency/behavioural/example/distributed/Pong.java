package sd.lab.agency.behavioural.example.distributed;

import sd.lab.agency.behavioural.Behaviour;
import sd.lab.agency.behavioural.impl.BaseBehaviouralAgent;
import sd.lab.agency.behavioural.messages.Message;

import static sd.lab.agency.behavioural.Behaviour.send;

class Pong extends BaseBehaviouralAgent {
    private final String pingName;

    private Behaviour mainBehaviour;
    private Behaviour closingBehaviour;

    public Pong(String name, String pongName) {
        super(name);
        this.pingName = pongName;
    }

    @Override
    public void setup() {
        mainBehaviour = Behaviour.receive(this::onMessageReceived)
                .andThen(send(pingName, "pong", this::onMessageSent))
                .repeatForEver()
                .addTo(this);

        // notice this behaviour is not added
        closingBehaviour = Behaviour.of(() -> log("My duty here is over. Good bye"))
                .andThen(this::stop);
    }

    private void onMessageSent(Message message) {
        log("Sent message %s", message);
    }

    private void onMessageReceived(Message message) {
        log("Received message %s", message);
        if (message.getSender().startsWith(pingName)) {
            if (message.getContent().equalsIgnoreCase("stop")) {
                removeBehaviour(mainBehaviour);
                addBehaviour(closingBehaviour);
                return;
            } else if (message.getContent().equalsIgnoreCase("ping")) {
                return;
            }
        }
        log("Wrong content or sender, I'm out.");
        stop();
    }
}
