package sd.lab.agency.behavioural.impl;

import sd.lab.agency.behavioural.Behaviour;
import sd.lab.agency.behavioural.BehaviouralAgent;
import sd.lab.agency.impl.BaseAgent;

import java.util.*;

public abstract class BaseBehaviouralAgent extends BaseAgent implements BehaviouralAgent {

    private final Queue<Behaviour> toDoList = new LinkedList<>();
    private final Set<Behaviour> toBeRemoved = new HashSet<>();  // TODO notice this!

    protected BaseBehaviouralAgent(String name) {
        super(name);
    }

    @Override
    public Queue<Behaviour> getToDoList() {
        return toDoList;
    }

    @Override
    public final void onBegin() {
        setup();
    }

    @Override
    public void onRun() throws Exception {
        if (toDoList.isEmpty()) {
            pause();
        } else {
            final Queue<Behaviour> skipped = new LinkedList<>();
            Behaviour behaviour = toDoList.poll();
            try {
                while (behaviour != null && behaviour.isPaused()) {
                    skipped.add(behaviour);
                    behaviour = toDoList.poll();
                }
                toBeRemoved.clear();  // TODO notice this!
                if (behaviour != null) {
                    behaviour.execute(this);
                } else {
                    pause();
                }
            } finally {
                if (behaviour != null && !behaviour.isOver()) {
                    toDoList.add(behaviour);
                }
                toDoList.addAll(skipped);
                toDoList.removeAll(toBeRemoved);  // TODO notice this!
            }
        }
    }

    @Override
    public void setup() {
        // does nothing by default
    }

    @Override
    public final void onEnd() {
        tearDown();
    }

    @Override
    public void tearDown() {
        // does nothing by default
    }

    @Override
    public BehaviouralAgent addBehaviour(Collection<? extends Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toDoList.addAll(behaviours);
            resumeIfPaused();
        }
        return this;
    }

    @Override
    public BehaviouralAgent removeBehaviour(Collection<? extends Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toDoList.removeAll(behaviours);
            toBeRemoved.addAll(behaviours); // TODO notice this!
            resumeIfPaused();
        }
        return this;
    }
}
