package sd.lab.agency.behavioural.impl;


import sd.lab.agency.behavioural.Behaviour;
import sd.lab.agency.behavioural.BehaviouralAgent;
import sd.lab.agency.behavioural.messages.Message;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.utils.Action2;

import java.util.concurrent.CompletableFuture;

public abstract class Receive extends LindaOperation<StringTuple> {

    // TODO notice method Environment::getFullNameFor

    @Override
    public final CompletableFuture<StringTuple> invokeOnTupleSpace(BehaviouralAgent agent, TextualSpace tupleSpace) {
        return tupleSpace.in(Message.getTemplateForRecipient(agent));
    }

    @Override
    public final String getTupleSpaceName(BehaviouralAgent agent) {
        return "inbox-" + agent.getFullName();
    }

    @Override
    public final void onResult(BehaviouralAgent agent, StringTuple result) throws Exception {
        final Message parsedMessage = Message.fromTuple(result);
        onMessageReceived(agent, parsedMessage);
    }

    public abstract void onMessageReceived(BehaviouralAgent agent, Message message) throws Exception;

    public static Receive of(Action2<BehaviouralAgent, Message, Exception> callback) {
        return new Receive() {
            @Override
            public void onMessageReceived(BehaviouralAgent agent, Message message) throws Exception {
                callback.execute(agent, message);
            }

            @Override
            public Behaviour deepClone() {
                return Receive.of(callback);
            }
        };
    }
}
