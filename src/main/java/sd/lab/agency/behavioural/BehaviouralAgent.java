package sd.lab.agency.behavioural;

import sd.lab.agency.Agent;

import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;

public interface BehaviouralAgent extends Agent {
    Queue<Behaviour> getToDoList();

    void setup();
    void tearDown();

    BehaviouralAgent addBehaviour(Collection<? extends Behaviour> behaviours);

    default BehaviouralAgent addBehaviour(Behaviour... behaviours) {
        return addBehaviour(Arrays.asList(behaviours));
    }

    BehaviouralAgent removeBehaviour(Collection<? extends Behaviour> behaviours);

    default BehaviouralAgent removeBehaviour(Behaviour... behaviours) {
        return removeBehaviour(Arrays.asList(behaviours));
    }

}
