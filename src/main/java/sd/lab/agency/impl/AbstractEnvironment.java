package sd.lab.agency.impl;

import sd.lab.agency.Agent;
import sd.lab.agency.Environment;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Stream;

public abstract class AbstractEnvironment implements Environment {
    private final ExecutorService engine;
    private final String name;
    private final Map<String, Agent> agents = new HashMap<>();

    public AbstractEnvironment(ExecutorService engine, String name) {
        this.engine = Objects.requireNonNull(engine);
        this.name = Optional.ofNullable(name).orElseGet(() -> getClass().getSimpleName() + "#" + System.identityHashCode(this));
    }

    public AbstractEnvironment(String name) {
        this(Executors.newCachedThreadPool(), name);
    }

    public AbstractEnvironment(ExecutorService engine) {
        this(engine, null);
    }

    public AbstractEnvironment() {
        this((String) null);
    }

    @Override
    public <A extends Agent> A createAgent(Class<A> agentClass, String name, Object... args) {
        if (agents.containsKey(name)) {
            throw new IllegalArgumentException(String.format("An agent named %s already exists in environment %s", name, getName()));
        }

        final Object[] arguments = Stream.concat(Stream.of(name), Stream.of(args)).toArray();

        final Optional<A> newAgent = Stream.of(agentClass.getConstructors())
                .filter(c -> c.getParameterCount() == arguments.length)
                .map(constructor -> {
                    constructor.setAccessible(true);
                    try {
                        final A agent = (A) constructor.newInstance(arguments);
                        return Optional.of(agent);
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                        throw new IllegalStateException(e);
                    }
                }).filter(Optional::isPresent)
                .findAny()
                .flatMap(Function.identity());

        if (!newAgent.isPresent()) {
            throw new IllegalArgumentException("No constructor for class " + agentClass.getName() + " accepts arguments: " + Arrays.toString(arguments));
        }

        return registerAgent(newAgent.get());

    }

    @Override
    public <A extends Agent> A registerAgent(A agent) {
        if (agents.containsKey(agent.getLocalName())) {
            throw new IllegalArgumentException(String.format("An agent named %s already exists in environment %s", agent.getLocalName(), getName()));
        }
        agent.setEnvironment(this);
        agents.put(agent.getLocalName(), agent);
        return agent;
    }

    @Override
    public Set<Agent> getAgents() {
        return new HashSet<>(agents.values());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Environment awaitAllAgentsStop(Duration duration) throws InterruptedException, ExecutionException, TimeoutException {
        for (Agent a : getAgents()) {
            a.await(duration);
        }
        return this;
    }

    @Override
    public Environment shutdown() {
        getEngine().shutdown();
        return this;
    }

    @Override
    public Environment awaitShutdown(Duration duration) throws InterruptedException {
        getEngine().awaitTermination(duration.toMillis(), TimeUnit.MILLISECONDS);
        return this;
    }

    @Override
    public ExecutorService getEngine() {
        return engine;
    }

    @Override
    public String getFullNameFor(String localOrFullName) {
        if (localOrFullName.contains("@")) {
            return localOrFullName;
        } else {
            return localOrFullName + "@" + getName();
        }
    }
}
