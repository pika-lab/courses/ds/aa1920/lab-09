package sd.lab.agency.impl;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.linda.textual.remote.RemoteTextualSpaceImpl;

import java.util.HashMap;
import java.util.Map;

public class DistributedEnvironment extends AbstractEnvironment {

    private final String host;
    private final int port;
    private final String mimeType;
    private final String username;
    private final String password;
    private final HttpClient httpClient = Vertx.vertx().createHttpClient(
            new HttpClientOptions()
                    .setMaxPoolSize(1 << 15)
                    .setHttp2MaxPoolSize(1 << 15)
    );
    private final Map<String, TextualSpace> textualSpaces = new HashMap<>();

    public DistributedEnvironment(String name, String host, int port, String mimeType, String username, String password) {
        super(name);
        this.host = host;
        this.port = port;
        this.mimeType = mimeType;
        this.username = username;
        this.password = password;
    }

    @Override
    public TextualSpace getTextualSpace(String name) {
        return textualSpaces.computeIfAbsent(name, n -> {
            try {
                return new RemoteTextualSpaceImpl(
                    httpClient,
                        host,
                        port,
                        name,
                        mimeType,
                        username,
                        password
                );
            } catch (Exception e) {
                e.printStackTrace();
                throw new IllegalStateException(e);
            }
        });
    }
}
