package sd.lab.ws.storage;

import sd.lab.ws.presentation.UserData;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

public class UserStorageImpl implements UserStorage {

    private final Set<UserData> users = new HashSet<>();

    public UserStorageImpl() {
        // add some default user
        register(
                new UserData()
                    .setRole(UserData.Role.ADMIN)
                    .setUsername("admin")
                    .setFullName("Administrator")
                    .setEmail("tuplespace.administrator@unibo.it")
                    .setPassword("admin")
                    .setId(UUID.randomUUID())
        );

        register(
                new UserData()
                        .setRole(UserData.Role.USER)
                        .setUsername("user")
                        .setFullName("User")
                        .setEmail("tuplespace.user@unibo.it")
                        .setPassword("user")
                        .setId(UUID.randomUUID())
        );
    }

    @Override
    public Optional<UserData> find(UserData minimal) {
        return users.stream().filter(it -> it.sameUserOf(minimal)).map(UserData::new).findAny();
    }

    @Override
    public Optional<UserData> findByIdentifier(String identifier) {
        return users.stream().filter(it -> it.isIdentifiedBy(identifier)).map(UserData::new).findAny();
    }

    @Override
    public UserData register(UserData newUser) {
        final Optional<UserData> existing = find(newUser);
        if (existing.isPresent()) {
            throw new IllegalArgumentException(
                    String.format(
                        "Some identifier of the new user %s conflicts with some identifier of the already registered user %s",
                        newUser,
                        existing.get()
                    )
            );
        }
        final UserData toBeAdded = new UserData(newUser).hashPassword();
        users.add(toBeAdded);
        return new UserData(toBeAdded);
    }

    @Override
    public UserData unregister(UserData existingUser) {
        final Optional<UserData> existing = find(existingUser);
        if (!existing.isPresent()) {
            throw new IllegalArgumentException(
                    String.format(
                            "No registered user shares its identifiers with %s",
                            existingUser
                    )
            );
        }
        users.remove(existing.get());
        return existing.get();
    }

    @Override
    public UserData update(UserData oldUser, UserData newUser) {
        final Optional<UserData> existing = find(oldUser);
        if (!existing.isPresent()) {
            throw new IllegalArgumentException(
                    String.format(
                            "No registered user shares its identifiers with %s",
                            oldUser
                    )
            );
        }
        users.remove(existing.get());
        final UserData toBeAdded = mergeUsers(existing.get(), newUser);
        if (newUser.getPassword() != null && !existing.get().getPassword().equals(newUser.getPassword())) {
            toBeAdded.hashPassword();
        }
        users.add(toBeAdded);
        return new UserData(toBeAdded);
    }

    private UserData mergeUsers(UserData currentUser, UserData newUserData) {
        final UserData edits = new UserData(newUserData).setId(null);
        final UserData result = new UserData(currentUser).setPropertiesToNonNullsOf(edits);
        return result;
    }

    @Override
    public Stream<UserData> getAll() {
        return users.stream().map(UserData::new);
    }
}
