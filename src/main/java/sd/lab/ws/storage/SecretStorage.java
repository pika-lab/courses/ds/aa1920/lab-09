package sd.lab.ws.storage;

public interface SecretStorage extends Storage {

    String getSecret();

    void setSecret(String secret);

    static SecretStorage getInstance() {
        return new SecretStorageImpl();
    }
}
