package sd.lab.ws.storage;

import java.util.Optional;

public class SecretStorageImpl implements SecretStorage {
    private static final String DEFAULT_SECRET = "this is not a secret";

    private String secret = DEFAULT_SECRET;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        secret = Optional.ofNullable(secret).orElse(DEFAULT_SECRET);
    }
}
