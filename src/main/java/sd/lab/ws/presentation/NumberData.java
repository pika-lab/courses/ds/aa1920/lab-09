package sd.lab.ws.presentation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.IOException;
import java.util.Objects;

@JacksonXmlRootElement(localName = "number")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NumberData extends Data {

    private java.lang.Number value = null;

    public NumberData() {

    }

    public NumberData(java.lang.Number value) {
        this.value = value;
    }

    public NumberData(NumberData clone) {
        this(clone.value);
    }

    @JsonProperty("value")
    @JacksonXmlProperty(localName = "value")
    public java.lang.Number getValue() {
        return value;
    }

    public NumberData setValue(java.lang.Number value) {
        this.value = value;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NumberData link = (NumberData) o;
        return Objects.equals(value, link.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "Link{" +
                "url='" + value + '\'' +
                '}';
    }

    public static NumberData fromJSON(String representation) throws IOException {
        return Data.fromJSON(representation, NumberData.class);
    }

    public static NumberData fromYAML(String representation) throws IOException {
        return Data.fromYAML(representation, NumberData.class);
    }

    public static NumberData fromXML(String representation) throws IOException {
        return Data.fromXML(representation, NumberData.class);
    }

    public static NumberData parse(String mimeType, String payload) throws IOException {
        return parse(mimeType, payload, NumberData.class);
    }

}
