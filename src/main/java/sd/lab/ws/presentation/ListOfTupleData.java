package sd.lab.ws.presentation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import sd.lab.linda.textual.StringTuple;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@JacksonXmlRootElement(localName = "listOfTuples")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListOfTupleData extends ListData<TupleData> {

    public ListOfTupleData() {
    }

    public ListOfTupleData(Collection<? extends TupleData> collection) {
        super(collection);
    }

    public ListOfTupleData(Stream<? extends TupleData> stream) {
        super(stream);
    }

    public ListOfTupleData(TupleData element1, TupleData... elements) {
        super(element1, elements);
    }

    @JsonProperty("tuples")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "tuple")
    public List<TupleData> getTuples() {
        return getItems();
    }

    public List<StringTuple> toListOfTuples() {
        return getTuples().stream().map(TupleData::toTuple).collect(Collectors.toList());
    }

    public ListOfTupleData setTuples(List<TupleData> users) {
        setItems(users);
        return this;
    }

    public static ListOfTupleData fromJSON(String representation) throws IOException {
        return Data.fromJSON(representation, ListOfTupleData.class);
    }

    public static ListOfTupleData fromYAML(String representation) throws IOException {
        return Data.fromYAML(representation, ListOfTupleData.class);
    }

    public static ListOfTupleData fromXML(String representation) throws IOException {
        return Data.fromXML(representation, ListOfTupleData.class);
    }

    public static ListOfTupleData parse(String mimeType, String payload) throws IOException {
        return parse(mimeType, payload, ListOfTupleData.class);
    }
}
