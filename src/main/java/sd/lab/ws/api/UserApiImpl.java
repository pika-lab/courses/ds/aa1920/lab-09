package sd.lab.ws.api;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.exceptions.ConflictError;
import sd.lab.ws.exceptions.ForbiddenError;
import sd.lab.ws.exceptions.NotFoundError;
import sd.lab.ws.presentation.LinkData;
import sd.lab.ws.presentation.ListOfUserData;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.UserStorage;

import java.util.Optional;
import java.util.stream.Stream;

class UserApiImpl extends AbstractApi implements UsersApi {

    UserApiImpl(RoutingContext routingContext, UserStorage userStorage, SecretStorage secretStorage) {
        super(routingContext, userStorage, secretStorage);
    }

    @Override
    public void createUser(UserData userData, Promise<? super LinkData> promise) {
        try {
            if (!getAuthenticatedUser().isPresent() || getAuthenticatedUser().get().getRole() == UserData.Role.USER) {
                userData.setRole(UserData.Role.USER);
            }
            getUserStorage().register(new UserData(userData));
            promise.complete(new LinkData("/" + userData.getUsername()));
        } catch (IllegalArgumentException e) {
            throw new ConflictError(e);
        }
    }

    @Override
    public void readAllUsers(Integer skip, Integer limit, String filter, Promise<? super ListOfUserData> promise) {
        ensureAuthenticatedUserAtLeast(UserData.Role.ADMIN);

        // beyond this point, we're sure that the client is at least an admin
        final Stream<UserData> selected = getUserStorage().getAll()
                .filter(it -> it.getEmail().contains(filter) || it.getUsername().contains(filter) || it.getFullName().contains(filter))
                .skip(skip)
                .limit(limit);

        promise.complete(new ListOfUserData(selected));
    }

    @Override
    public void readUser(String identifier, Promise<? super UserData> promise) {
        ensureAuthenticatedUserAtLeast(UserData.Role.USER);

        // beyond this point, we're sure that the client is at least a user

        final Optional<UserData> user = getUserStorage().findByIdentifier(identifier);
        if (user.isPresent()) {
            promise.complete(user.get());
        } else {
            throw new NotFoundError();
        }
    }

    @Override
    public void updateUser(String identifier, UserData newUserData, Promise<? super UserData> promise) {
        ensureAuthenticatedUserAtLeast(UserData.Role.USER);

        getAuthenticatedUser().ifPresent(u -> {
            if (!u.isIdentifiedBy(identifier)) { // user data can only be updated by its owner
                throw new ForbiddenError();
            }
        });

        final  UserData updatedUser = getUserStorage().update(getAuthenticatedUser().get(), newUserData);
        promise.complete(updatedUser);
    }




}
