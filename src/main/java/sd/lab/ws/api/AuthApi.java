package sd.lab.ws.api;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.auth.JsonWebToken;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.UserStorage;

public interface AuthApi extends Api {

    void createToken(UserData credentials, Integer duration, Promise<? super JsonWebToken> promise);

    static AuthApi get(RoutingContext context, UserStorage userStorage, SecretStorage secretStorage) {
        return new AuthApiImpl(context, userStorage, secretStorage);
    }
}