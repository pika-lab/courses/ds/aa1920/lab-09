package sd.lab.ws.api;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.auth.JsonWebToken;
import sd.lab.ws.exceptions.NotFoundError;
import sd.lab.ws.exceptions.UnauthorizedError;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.UserStorage;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

public class AuthApiImpl extends AbstractApi implements AuthApi {

    public AuthApiImpl(RoutingContext routingContext, UserStorage userStorage, SecretStorage secretStorage) {
        super(routingContext, userStorage, secretStorage);
    }

    @Override
    public void createToken(UserData credentials, Integer duration, Promise<? super JsonWebToken> promise) {
        final Optional<UserData> optUser = getUserStorage().find(credentials);
        if (optUser.isPresent()) {
            final UserData user = optUser.get();
            if (user.checkPassword(credentials.getPassword())) {
                promise.complete(generateToken(user, duration));
            } else {
                promise.fail(new UnauthorizedError());
            }
        } else {
            promise.fail(new UnauthorizedError());
        }
    }

    private JsonWebToken generateToken(UserData user, Integer duration) {
        final OffsetDateTime now = OffsetDateTime.now();

        final JsonWebToken jwt = new JsonWebToken();
        jwt.getHeader()
                .setAlg(JsonWebToken.Header.ALG_SIMPLE_HMAC)
                .setTyp(JsonWebToken.Header.TYP_JWT);
        jwt.getPayload()
                .setJti(UUID.randomUUID().toString())
                .setIat(now)
                .setSub("/" + user.getUsername())
                .setUser(new UserData(user).setPassword(null));

        if (duration != null) {
            jwt.getPayload().setExp(now.plus(Duration.ofSeconds(duration)));
        }

        return jwt;
    }
}
