package sd.lab.ws.api;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.ws.exceptions.UnauthorizedError;
import sd.lab.ws.presentation.*;
import sd.lab.ws.storage.SecretStorage;
import sd.lab.ws.storage.TextualSpaceStorage;
import sd.lab.ws.storage.UserStorage;

import java.util.Optional;

public class TupleSpaceApiImpl extends AbstractApi implements TupleSpaceApi {
    
    private final TextualSpaceStorage textualSpaceStorage;
    
    TupleSpaceApiImpl(RoutingContext routingContext, UserStorage userStorage, SecretStorage secretStorage, TextualSpaceStorage tsStorage) {
        super(routingContext, userStorage, secretStorage);
        textualSpaceStorage = tsStorage;
    }

    private TextualSpace getTextualSpaceForLoggedUser(String tupleSpaceName) {
        if (!getTextualSpaceStorage().contains(tupleSpaceName)) {
            ensureAuthenticatedUserAtLeast(UserData.Role.ADMIN);
        }

        return getTextualSpaceStorage().createIfAbsentOrGet(tupleSpaceName);
    }

    @Override
    public void getAllTuples(String tupleSpaceName, Promise<? super ListOfTupleData> promise) {
//        final TextualSpace ts = getTextualSpaceStorage().createIfAbsentOrGet(tupleSpaceName);
        final TextualSpace ts = getTextualSpaceForLoggedUser(tupleSpaceName);
        ts.get().whenComplete((tuples, error) -> {
            if (error != null) {
                promise.fail(error);
            } else {
                promise.complete(new ListOfTupleData(tuples.stream().map(TupleData::new)));
            }
        });
    }

    @Override
    public void countAllTuples(String tupleSpaceName, Promise<? super NumberData> promise) {
        final TextualSpace ts = getTextualSpaceForLoggedUser(tupleSpaceName);
        ts.getSize().whenComplete((count, error) -> {
            if (error != null) {
                promise.fail(error);
            } else {
                promise.complete(new NumberData(count));
            }
        });
    }

    @Override
    public void insertTuple(String tupleSpaceName, StringTuple tuple, Promise<? super TupleData> promise) {
        final TextualSpace ts = getTextualSpaceForLoggedUser(tupleSpaceName);
        ts.out(tuple).whenComplete((t, error) -> {
            if (error != null) {
                promise.fail(error);
            } else {
                promise.complete(new TupleData(t));
            }
        });
    }

    @Override
    public void consumeTuple(String tupleSpaceName, RegexTemplate template, Promise<? super TupleData> promise) {
        final TextualSpace ts = getTextualSpaceForLoggedUser(tupleSpaceName);
        ts.in(template).whenComplete((tuple, error) -> {
            if (error != null) {
                promise.fail(error);
            } else {
                promise.complete(new TupleData(tuple));
            }
        });
    }

    @Override
    public void observeTuple(String tupleSpaceName, RegexTemplate template, Promise<? super TupleData> promise) {
        final TextualSpace ts = getTextualSpaceForLoggedUser(tupleSpaceName);
        ts.rd(template).whenComplete((tuple, error) -> {
            if (error != null) {
                promise.fail(error);
            } else {
                promise.complete(new TupleData(tuple));
            }
        });
    }

    @Override
    public void getAllTupleSpaces(Integer skip, Integer limit, String filter, Promise<? super ListOfLinkData> promise) {
        final ListOfLinkData result = new ListOfLinkData(
                getTextualSpaceStorage().getAll()
                        .map(TextualSpace::getName)
                        .filter(name -> name.contains(filter))
                        .skip(skip)
                        .limit(limit)
                        .map(name -> "/" + name) // the reminder of the path will be injected by the route
                        .map(LinkData::new)
        );
        
        promise.complete(result);
    }

    public TextualSpaceStorage getTextualSpaceStorage() {
        return textualSpaceStorage;
    }
}
