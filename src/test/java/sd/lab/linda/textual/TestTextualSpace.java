package sd.lab.linda.textual;

import java.util.concurrent.ExecutorService;

public class TestTextualSpace extends AbstractTestTextualSpace {
    @Override
    protected TextualSpace getTupleSpace(ExecutorService executor) {
        return TextualSpace.of(executor);
    }
}
