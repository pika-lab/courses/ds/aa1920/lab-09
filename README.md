## Example instructions

1. Start a service

```bash
./gradlew runService
```

2. Start the Ping agent

```bash
./gradlew runPing
```

3. Start the Pong agent

```bash
./gradlew runPong
```